+++
date = "2016-11-05T21:05:33+05:30"
title = "About me"
+++

Hi, I am a developer, designer, and researcher with 4 years experience in  Front End Development, Design, and UX and  and 3 years in regulatory compliance consulting. Master Science Human-Computer Interaction GIT. Currently moving to the Denver-Boulder area and pursuing IAAP-CPAAC accessibility certification.

![](/img/portrait.jpg)

I believe in working in the medium of our creations. As a painter knows their brushes and oils, a carpenter their tools and materials, or a chef their foods and flavors–the quality of our designs result from the meticulous care we put into each piece of the materials we work with. In the world of highly complex technologies and problems, every interface, interaction, every task and problem, every pixel and line of code has a story to tell. Let's make it a good one.

#### Work Experience
* Researcher/Developer – Sonification Lab, Georgia Institute of Technology, August 2016 (Grad student) - May 2018 (Tech Temp) - (current)
* UX Intern – Lucena Research and Quantitative Analytics, August 2017 - (current)
* ICT Accessibility Auditor (Graduate Temp) – AMAC Accessibility, Georgia Institute of Technology, July 2017 - August 2017
* Web / Graphic Designer – City Publications, August 2014 - August 2016
* Marketing & Design Intern – Strategies Group (Sage Group subsidiary), May 2014 - August 2014
* Geo-technical Laboratory Assistant Tech – Oasis Consulting, January 2011 - May 2014

#### Education

* Master of Science in Human-Computer Interaction, Georgia Institute of Technology 2018
* Bachelor of Arts in Graphic-Communications, Reinhardt University 2014, Magna Cum Laude & Academic Honors
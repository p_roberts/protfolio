+++
showonlyimage = false
draft = false
image = "img/portfolio/ndaqscreen.png"
date = "2016-11-05T18:25:22+05:30"
title = "Nasdaq Investment Strategy Intranet"
weight = 1
+++

Nasdaq Analytics Hub resource center for machine learning enhanced investment strategies.
<!--more-->

For this project we were approached by the Nasdaq Analytics Hub to help provide a resource and information center for their sales people. This would be a one-stop-shop where they could educate themselves about investment strategies, enhanced with machine learning by Lucena, to sell to investment companies.

![landing page for Nasdaq Report Center](/portfolio/img/splash.png)

After an initial conversation with our stakeholders, we diagramed the structure of the expected information we would be reporting and low fidelity wireframes to convey and confirm the proper data to display and a suitable layout.

![tree view of files](/portfolio/img/tree.png)

![lowfi wireframe 1](/portfolio/img/LowFi_v2.png)

![lowfi wireframe 1](/portfolio/img/LowFi.png)

The initial concept was approved and we moved forward with a hi-fidelity mock of the same content.

![hifi wireframe](/portfolio/img/n3.png)
providers page

## Development

As part of our initiative to move to some more modern front-end frameworks, the view layer for this intranet was developed with React.js and connects to a simple API back-end that validates the user and delivers daily updated investment strategy information based upon their access credentials. This web app uses simple routing and password protected login, and Redux to persist state between route views.

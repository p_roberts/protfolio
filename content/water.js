let node = {
  _x: null,
  _y: null,
  setPosition: function (x, y) {
    this._x = x
    this._y = y
  },
  logPosition: function () {
    console.log(`this x: ${this.x}\nthis y: ${this.y}`)
  }
}
const rows = 200
const cols = 200

let pool = [[[]]]
for (let i = 0; i < rows; i++) {
  pool[i] = []
  for (let j = 0; j < cols; j++) {
    pool[i].push(new node.setPosition(i,j))
    console.log(pool[i][j])
  }
}

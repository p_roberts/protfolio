+++
date = "2016-11-05T21:05:33+05:30"
title = "Personal Logo Animation"
svg = "img/nnet.svg"
weight = 2
+++

An svg animation for my personal branding.
<!--more-->

<svg width="200px" height="260px" viewBox="0 0 200 250" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
  <style>
    .fadein {
      animation: fadein 3s ease 5s forwards;
      opacity: 0;
    }
    @keyframes fadein {
      0% {
      opacity: 0;
      }
      100% {
      opacity: 100;
      }
    }
    #path1 {
      stroke-dasharray: 800;
      stroke-dashoffset: 800;
      animation: dash 1s ease-in forwards;
      animation-delay: 0.7s;
      stroke: #1976D2;
      box-shadow: 1px 2px 3px #fff;
      backface-visibility: hidden;
    }
    @keyframes dash  {
      from {
        stroke-dashoffset: 800;
      }
      to {
        stroke-dashoffset: 0;
      }
    }
    #path2 {
      stroke-dasharray: 800;
      stroke-dashoffset: 800;
      animation: dash 2s ease-in forwards;
      animation-delay: 1.2s;
      stroke: #1976D2;
      backface-visibility: hidden;
    }
    #path3 {
      stroke-dasharray: 800;
      stroke-dashoffset: 800;
      animation: dash 2s ease-in forwards;
      animation-delay: 2.8s;
      stroke: #1976D2;
      backface-visibility: hidden;
    }
    #path4 {
      stroke-dasharray: 800;
      stroke-dashoffset: 800;
      animation: dash 2s ease-in forwards;
      animation-delay: 3.3s;
      stroke: #1976D2;
      backface-visibility: hidden;
    }
  </style>
  <g stroke-width="1" fill="none" fill-rule="evenodd"   backface-visibility="hidden">
    <path
      d="M36.3333333,250 L36,98"
      id="path1"
      stroke-width="8"
      stroke-linecap="round"
    ></path>
    <path
      d="M112.761348,38 C184.825712,48.8548783 178.607418,163 98.9591813,163 C20.0095707,163 15.1571029,50.5367111 85.2703036,38"
      id="path2" stroke-width="8" stroke-linecap="round"></path>
    <path
      d="M100,75 C100,25.6666667 100,1 100,1"
      id="path3"
      stroke-width="8"
      stroke-linecap="round"
    ></path>
    <path
      d="M190,216 L145,145"
      id="path4"
      stroke-width="8"
      stroke-linecap="round"
    ></path>
  </g>
</svg>
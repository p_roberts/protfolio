let newText = ""
const writeText = (text, target, index = 0) => {
  if (index == 0) {
    newText = ""
  }
    setTimeout(() => {
      newText += text[index]
      target.innerHTML = newText
      index++
      if (index < text.length) {
        writeText(text, target, index)
      }
    }, 50);
}
window.onload = setInterval(() => {
  let textbox = document.getElementsByTagName("tspan")
  let tspan = textbox[0]
  writeText("Loading...", tspan)
}, 2000);